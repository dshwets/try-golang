package main

import (
	"fmt"
	"github.com/beevik/ntp"
)

func main() {
	ntpTime, err := ntp.Time("0.beevik-ntp.pool.ntp.org")

	if err != nil {
		fmt.Println(err)
	}
	//currtimestr := strings.Join()
	hours, minutes, seconds := ntpTime.Clock()
	fmt.Printf("Current time: %v:%v:%v", hours, minutes, seconds)

}
