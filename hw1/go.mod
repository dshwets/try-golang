module hw1

go 1.16

require (
	github.com/beevik/ntp v0.3.0 // indirect
	github.com/golangci/golangci-lint v1.38.0 // indirect
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4 // indirect
)
